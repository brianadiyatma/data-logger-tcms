package model

type VehicleData struct {
	ObjectDetection    string  `json:"object_detection"`
	Latitude           float64 `json:"latitude"`
	Longitude          float64 `json:"longitude"`
	RFIDTag            string  `json:"rfid_tag"`
	Powering           string  `json:"powering"`
	Braking            string  `json:"braking"`
	Speed              float64 `json:"speed"`
	Direction          string  `json:"direction"`
	ErrorRFID          string  `json:"error_rfid"`
	NetworkStatus      string  `json:"network_status"`
	ChooseMode         string  `json:"choose_mode"`
	AutonomousApproval bool    `json:"autonomous_approval"`
}