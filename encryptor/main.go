package main

import (
	"encoding/json"
	"encryptor-tcms/cryptography"
	"encryptor-tcms/model"
	"encryptor-tcms/stream"
	"fmt"
)

func main() {

	client := stream.MqttConnClient()
	pub := stream.NewPublisher(client)

	data := model.VehicleData{}
	jsonData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Error marshalling JSON:", err)
		return
	}
	key := []byte("thisis32byteslongpassphraseimusi")
	encryptedData, err := cryptography.EncrpytSHA256(jsonData, key)
	fmt.Printf("Encrypted Data: %s\n", encryptedData)
	if err != nil {
		panic(err)
	}
	pub.Publish("TRAIN_1",encryptedData)

	
	fmt.Print("Encryptor is running")
}