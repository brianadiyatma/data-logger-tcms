package stream

import (
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)



type Publisher struct {
	client mqtt.Client
}

func NewPublisher (mqttClient mqtt.Client)  *Publisher {
	return &Publisher{
		client: mqttClient,
	}
}

func (p *Publisher) Publish(topic string ,val interface{}) {
	token := p.client.Publish(topic, 0, false,val)
	token.Wait() 
	if token.Error() != nil {
		fmt.Println("ERROR PUBLISHING PAYLOAD")
	}
}