package main

import (
	"decryptor-tcms/stream"
	"os"
	"os/signal"
	"syscall"
	"log"
)

func main() {
	client := stream.MqttConnClient()
	sub := stream.NewSubscriber(client)
	sub.Subscribe("TRAIN_1")

	// Create a channel to listen for interrupt or terminate signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// Block until a signal is received
	log.Println("Subscribed to TRAIN_1. Waiting for signal to terminate...")
	<-sigs
	log.Println("Shutting down...")
}
