package stream

import (
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)


var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
    fmt.Println("Connected to INKA MQTT Broker")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
    fmt.Printf("Connect lost: %v", err)
}

func MqttConnClient() mqtt.Client {
	opts := mqtt.NewClientOptions()
	opts.AddBroker("mqtt://103.162.253.243:1883")
	opts.SetUsername("refe")
	opts.SetPassword("Tr4N5F0Rm3R")
	opts.OnConnect = connectHandler
    opts.OnConnectionLost = connectLostHandler

	client := mqtt.NewClient(opts)

	if token := client.Connect(); token.Wait() && token.Error() != nil {
        panic(token.Error())
    }
	
	return client
}