package stream

import (
	"decryptor-tcms/cryptography"
	"fmt"
	"log"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)



type Subscriber struct {
	client mqtt.Client
}

func NewSubscriber (mqttClient mqtt.Client)  *Subscriber {
	return &Subscriber{
		client: mqttClient,
	}
}

func (p *Subscriber) Subscribe(topic string ) {
	token := p.client.Subscribe(topic,0, func(c mqtt.Client, m mqtt.Message) {
		key := []byte("thisis32byteslongpassphraseimusi")
		decryptedData, err := cryptography.DecryptJSON(m.Payload(),key)
		if err != nil {
			log.Fatalf("Error decrypting JSON: %v", err)
		}
		fmt.Printf("Decrypted Data: %s\n", decryptedData)

	})
	if token.Error() != nil {
		fmt.Println("ERROR PUBLISHING PAYLOAD")
	}
}